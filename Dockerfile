FROM jupyter/datascience-notebook:latest

# octave
# https://anaconda.org/conda-forge/octave_kernel
RUN conda config --add channels conda-forge
RUN conda config --add channels bioconda

RUN conda install --yes \
    'gnuplot' \
    'ghostscript' \
    'octave' \
    'octave_kernel'

## docs
USER root
RUN apt-get update
RUN apt-get install --yes texinfo
RUN apt-get clean
USER jovyan

# open street map 
# http://geoffboeing.com/2016/11/osmnx-python-street-networks/
RUN conda install --yes \
    'osmnx'

# lxml and html5lib for beautifulsoup HTML parsing
RUN conda install --yes html5lib lxml

# cartopy for drawing maps
RUN conda install --yes cartopy

# clean everything up
RUN conda clean -tipsy

# remove unnecessary directory
RUN rmdir /home/jovyan/work

# helpful python  bits
RUN pip install tqdm

# add a welcome README.md
ADD --chown=1000:1000 Welcome.ipynb /home/jovyan/Welcome.ipynb
